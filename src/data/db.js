const data = {
  usuarios: [
    {
      id: 1,
      nombre: "Abu Dhabi",
      email: "abu.dhabi@gmail.com",
      telefono: "987654321",
    },
    {
      id: 2,
      nombre: "Blanc A. Varella",
      email: "blanc.varella@gmail.com",
      telefono: "987654321",
    },
    {
      id: 3,
      nombre: "Celia Cruz",
      email: "celia.cruz@gmail.com",
      telefono: "987654321",
    },
    {
      id: 4,
      nombre: "Dionisio Limones",
      email: "dionisio.limones@gmail.com",
      telefono: "987654321",
    },
    {
      id: 5,
      nombre: "Ernesto Merino",
      email: "ernesto.merino@gmail.com",
      telefono: "987654321",
    },
  ],
  propuestas: [
    {
      id: "1",
      titulo: "Sistema de Innovación Educativa",
      descripcion:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      imageSource:
        "https://images.pexels.com/photos/159711/books-bookstore-book-reading-159711.jpeg",
      imageAlt: "imagen del sistema de innovacion educativa",
    },
    {
      id: "2",
      titulo: "Gadget policial de geo-reporte",
      descripcion:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      imageSource:
        "https://images.pexels.com/photos/1464230/pexels-photo-1464230.jpeg",
      imageAlt: "imagen del gadget policial de geo reporte",
    },
    {
      id: "3",
      titulo: "Identificación de patrones de habla en niños a través de I.A.",
      descripcion:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      imageSource:
        "https://images.pexels.com/photos/257904/pexels-photo-257904.jpeg",
      imageAlt:
        "imagen del sistema de identificacion de patrones con inteligencia artificial",
    },
    {
      id: "4",
      titulo: "Evaluación de radiografías con visión artificial",
      descripcion:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      imageSource:
        "https://images.pexels.com/photos/8566472/pexels-photo-8566472.jpeg",
      imageAlt:
        "imagen del sistema de evaluación de radiografías con visión artificial",
    },
    {
      id: "5",
      titulo: "Sistema web de donaciones anónimas a pacientes de hospitales",
      descripcion:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      imageSource:
        "https://images.pexels.com/photos/3769151/pexels-photo-3769151.jpeg",
      imageAlt: "imagen del sistema web de donaciones anónimas a pacientes",
    },
  ],
  inversores: [
    {
      id: 1,
      nombre: "Abu Dhabi",
      email: "abu.dhabi@gmail.com",
      telefono: "987654321",
    },
    {
      id: 2,
      nombre: "Blanc A. Varella",
      email: "blanc.varella@gmail.com",
      telefono: "987654321",
    },
    {
      id: 3,
      nombre: "Celia Cruz",
      email: "celia.cruz@gmail.com",
      telefono: "987654321",
    },
    {
      id: 4,
      nombre: "Dionisio Limones",
      email: "dionisio.limones@gmail.com",
      telefono: "987654321",
    },
    {
      id: 5,
      nombre: "Ernesto Merino",
      email: "ernesto.merino@gmail.com",
      telefono: "987654321",
    },
  ],
};

export default data;
