import { Route, Routes } from "react-router-dom";

import routes from "./routes";

import LandingPage from "./pages/LandingPage/LandingPage";
import PropsPage from "./pages/PropsPage/PropsPage";

import Layout from "./components/Layout";
import DashboardPage from "./pages/DashboardPage/DashboardPage";

export default function App() {
  return (
    <Layout>
      <Routes>
        <Route path={routes.landing} element={<LandingPage />} />
        <Route path={routes.propuestas} element={<PropsPage />} />
        <Route path={routes.dashboard} element={<DashboardPage />} />
      </Routes>
    </Layout>
  );
}
