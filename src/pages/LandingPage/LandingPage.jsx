import illustration from "./Illustration.png";
import blob from "./blob.svg";

import PrimButton from "../../components/ui/PrimButton";

import classes from "./LandingPage.module.css";

export default function LandingPage() {
  return (
    <div className="relative">
      {/* Contenedor izquierda */}
      <div className="absolute w-full lg:w-1/2 z-20">
        <div className="py-10 ml-8">
          <div style={{ height: "20vh" }}></div>
          {/* Texto */}
          <h1 className="text-4xl lg:text-8xl text-center">
            <span className={classes.headline}>
              LOS INVERSIONISTAS TE ESPERAN
            </span>
          </h1>
          {/* SubTexto */}
          <p className="text-2xl mt-10">
            Coloca tu propuesta de emprendimiento para tener inversionistas
          </p>
          {/* Boton */}
          <div className="mt-10 w-1/6">
            <PrimButton label="EMPRENDER" />
          </div>
        </div>
      </div>

      {/* Illustration */}
      <div className="absolute top-20 z-10">
        <div className="flex flex-row-reverse">
          <img className="w-full lg:w-3/5" src={illustration} alt="" />
        </div>
      </div>

      <div className="absolute top-24 right-44 z-0">
        <img src={blob} alt="" />
      </div>
    </div>
  );
}
