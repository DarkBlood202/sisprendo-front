import SideBar from "../../components/dashboard/SideBar";

function DashboardPage() {
  return (
    <div>
      <SideBar />
    </div>
  );
}

export default DashboardPage;
