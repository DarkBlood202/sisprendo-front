import PropList from "../../components/propuestas/PropList";

import data from "../../data/db";

export default function PropsPage() {
  return (
    <div>
      <h1 className="font-bold text-4xl py-6 px-8">Propuestas</h1>
      <div className="mx-8 my-4">
        <PropList propuestas={data.propuestas} />
      </div>
    </div>
  );
}
