const routes = {
  landing: "/landing",
  propuestas: "/propuestas",
  dashboard: "/dashboard",
};

export default routes;
