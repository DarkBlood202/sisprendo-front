export default function SecButton(props) {
  return (
    <button className="bg-[#F3F1FF] p-2 rounded-lg w-full text-center font-bold text-[#816AFE]">
      {props.label}
    </button>
  );
}
