export default function SearchBar(props) {
  return (
    <div className="bg-gray-50 px-4 py-1 rounded-md flex gap-x-2 items-center">
      <i className="fas fa-search mr-2"></i>
      <input
        type="text"
        placeholder={props.placeholder}
        className="inline bg-gray-50 w-full outline-none"
      />
    </div>
  );
}
