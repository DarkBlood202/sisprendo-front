export default function Card(props) {
  return (
    <div className="bg-white p-2 rounded-lg shadow-md">{props.children}</div>
  );
}
