import logo from "./logo.svg";

import PrimButton from "./PrimButton";
import SecButton from "./SecButton";
import SearchBar from "./SearchBar";

import { Link } from "react-router-dom";

import routes from "../../routes";

export default function NavBar() {
  return (
    <div className="bg-white flex p-8 items-center justify-items-center shadow-md gap-x-8">
      {/* Logo izq */}
      <div className="flex-grow-0 flex-shrink-0">
        <img src={logo} alt="sisprendo logo" />
      </div>
      {/* Opciones de centro */}
      <div className="w-full">
        <div className="grid grid-cols-5 font-bold px-4 gap-x-6">
          <Link to={routes.landing}>Inicio</Link>
          <Link to={routes.propuestas}>Propuestas</Link>
          <div className="col-span-3">
            <SearchBar placeholder="Buscar..." />
          </div>
        </div>
      </div>
      {/* Derecha */}
      <div className="flex w-1/2 gap-x-4">
        <PrimButton label="Subir propuesta" />
        <Link to={routes.dashboard}>
          <SecButton label="Iniciar sesión" />
        </Link>
        <PrimButton label="Registro" />
      </div>
    </div>
  );
}
