export default function PrimButton(props) {
  return (
    <button className="bg-[#816AFE] p-2 rounded-lg w-full text-center font-bold text-white">
      {props.label}
    </button>
  );
}
