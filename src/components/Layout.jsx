import NavBar from "./ui/NavBar";

function Layout(props) {
  return (
    <div>
      <NavBar />
      <div>{props.children}</div>
    </div>
  );
}

export default Layout;
