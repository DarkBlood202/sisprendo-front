import React from "react";

function PropCard(props) {
  return (
    <div className="bg-white rounded-lg shadow-lg shadow-drop-2-center cursor-pointer">
      <img
        src={props.imageSource}
        alt={props.imageAlt}
        className="rounded-t-lg"
      />
      <div className="p-4">
        <h1 className="font-bold text-lg">{props.titulo}</h1>
      </div>
    </div>
  );
}

export default PropCard;
