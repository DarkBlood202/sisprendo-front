import PrimButton from "../ui/PrimButton";

function PropDetails(props) {
  return (
    <div className="bg-white rounded-lg grid grid-cols-2">
      <img
        src={props.imageSource}
        alt={props.imageAlt}
        className="w-full rounded-l-lg"
      />
      <div className="p-8">
        <h1 className="font-bold text-2xl">{props.titulo}</h1>
        <p className="my-4">{props.descripcion}</p>
        <PrimButton label="Aceptar propuesta" />
      </div>
    </div>
  );
}

export default PropDetails;
