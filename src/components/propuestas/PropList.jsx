import React from "react";

import PropCard from "./PropCard";
import PropDetails from "./PropDetails";

function PropList(props) {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-6 gap-8">
      {props.propuestas.map((propuesta) => (
        <PropCard
          titulo={propuesta.titulo}
          imageSource={propuesta.imageSource}
          imageAlt={propuesta.imageAlt}
        />
      ))}
    </div>
  );
}

export default PropList;
