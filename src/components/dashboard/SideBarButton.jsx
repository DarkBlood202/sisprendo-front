import React from "react";

function SideBarButton(props) {
  let active = (
    <div className="rounded-full h-2 w-2 text-transparent bg-white inline-block">
      .
    </div>
  );

  return (
    <div className="p-4 rounded-lg hover:bg-[#2BA4D9] cursor-pointer">
      <img src={props.iconSrc} className="inline mr-8" />
      <span>{props.label}</span>
      {props.active && active}
    </div>
  );
}

export default SideBarButton;
