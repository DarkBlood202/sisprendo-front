import SideBarButton from "./SideBarButton";

import inversIcon from "./icon-invers.svg";
import propsIcon from "./icon-props.svg";
import perfilIcon from "./icon-perfil.svg";

function SideBar() {
  return (
    <div className="bg-[#0091D1] text-white p-8 w-1/4 h-screen">
      <SideBarButton iconSrc={perfilIcon} label="Perfil" />
      <SideBarButton iconSrc={propsIcon} label="Mis propuestas" active />
      <SideBarButton iconSrc={inversIcon} label="Inversionistas" />
    </div>
  );
}

export default SideBar;
